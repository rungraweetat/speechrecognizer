package com.adpter

import android.annotation.SuppressLint
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.speech.SpeechRecognizer
import android.view.View
import android.widget.Button
import android.widget.TextView
import android.speech.RecognizerIntent
import android.content.Intent
import android.os.Handler
import android.speech.RecognitionListener
import android.util.Log
import kotlinx.android.synthetic.main.activity_main.*
import java.util.*


class MainActivity : AppCompatActivity() , View.OnClickListener {

    private lateinit var tvWording:TextView
    private lateinit var tvTimer:TextView
    private lateinit var btnStart:Button
    private lateinit var speechRecognizer: SpeechRecognizer
    private lateinit var lstWords: ArrayList<String>

    private var count = 0
    private var countWord = 0;
    private var handler:Handler = Handler()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        btnStart = findViewById(R.id.btnStart)
        tvWording = findViewById(R.id.tvWording)
        tvTimer = findViewById(R.id.tvTimer)

        btnStart.setOnClickListener(this)
        speechRecognizer = SpeechRecognizer.createSpeechRecognizer(this)
        speechRecognizer.setRecognitionListener(listenerSpeech())

        lstWords.add("เช้าฟาดผัดฟักเย็นฟาดผักฟัด");
        lstWords.add("ยายมีเลี้ยงหอยยายมอยเลี้ยงหมี");
        lstWords.add("ระนองระยองยะลา");
        lstWords.add("หมอนลอยน้ำมาว่ายน้ำไปถอยหมอน");
        lstWords.add("รถยนต์ล้อยางรถรางล้อเหล็ก");
        lstWords.shuffle()

        val timer = Timer()
        timer.scheduleAtFixedRate(object : TimerTask() {
            override fun run() {
                runOnUiThread {
                    if( lstWords.size > 0 )
                    {
                        tvTimer.text = ("count=$count")
                        count++
                        if( countWord < lstWords.size )
                            countWord++

                        lstWords.removeAt(0)
                        lstWords.shuffle()
                    }


                }
            }
        }, 1000, 1000)
    }

    internal inner class listenerSpeech : RecognitionListener {
        override fun onReadyForSpeech(params: Bundle) {
            Log.d("", "onReadyForSpeech")
        }

        override fun onBeginningOfSpeech() {
            Log.d("", "onBeginningOfSpeech")
        }

        override fun onRmsChanged(rmsdB: Float) {
            Log.d("", "onRmsChanged")
        }

        override fun onBufferReceived(buffer: ByteArray) {
            Log.d("", "onBufferReceived")
        }

        override fun onEndOfSpeech() {
            Log.d("", "onEndofSpeech")
        }

        @SuppressLint("SetTextI18n")
        override fun onError(error: Int) {
            Log.d("", "error $error")
            tvRecognizer.text = "error $error"
        }

        override fun onResults(results: Bundle) {
            var str = String()
            Log.d("", "onResults $results")

            val data = results.getStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION)
            for (i in 0 until data!!.size) {
                Log.d("", "result " + data[i])
                str += data[i]
            }
            tvRecognizer.text = str
        }

        override fun onPartialResults(partialResults: Bundle) {
            Log.d("", "onPartialResults")
        }

        override fun onEvent(eventType: Int, params: Bundle) {
            Log.d("", "onEvent $eventType")
        }
    }

    override fun onClick(v: View) {
        val intent = Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH)
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM)
        intent.putExtra(RecognizerIntent.EXTRA_CALLING_PACKAGE, "voice.recognition.test")

        intent.putExtra(RecognizerIntent.EXTRA_MAX_RESULTS, 5)
        speechRecognizer.startListening(intent)
    }
}
